import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'users' },
  {
    path: 'authentication',
    loadChildren:() => import("./modules/authentication/authentication.module").then(m => m.AuthenticationModule)
  },
  {
    path: 'users',
    loadChildren:() => import("./modules/users/users.module").then(m => m.UsersModule)
  },
  {
    path: 'posts',
    loadChildren:() => import("./modules/posts/posts.module").then(m => m.PostsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
