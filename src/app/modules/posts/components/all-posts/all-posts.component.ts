import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ipost } from 'src/app/modules/users/shared/ipost';

import { PostService } from '../../service/post.service';

@Component({
  selector: 'app-all-posts',
  templateUrl: './all-posts.component.html',
  styleUrls: ['./all-posts.component.scss']
})
export class AllPostsComponent implements OnInit {
  
posts:Ipost[] = [];


  constructor(private postservice:PostService, private router:Router) { }

  ngOnInit(): void {

    this.getPosts()

  }


  getPosts() {
    this.postservice.getPosts().subscribe(
      (res)=> {
        this.posts=res
        console.log(this.posts)
      }
    )
  }

}
