export interface Iuser {

  id?: string,
  name: string,
  email: string,
  username: string,
  website: string,
  phone: string,
  address: {
    city: string,
    geo:{
      lat: string,
      lng: string
  },
  street: string,
  suite: string,
  zipcode: string
  },
  company: {
    name: string,
    bs: string,
    catchPhrase: string
  }

}

