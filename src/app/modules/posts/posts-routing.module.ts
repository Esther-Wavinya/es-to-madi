import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllPostsComponent } from './components/all-posts/all-posts.component';


const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'all-posts' },
  {
    path: 'all-posts',
    component: AllPostsComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
