import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Iuser } from '../shared/iuser';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http:HttpClient) { }

  getUsers():Observable<Iuser[]> {
    return this.http.get<Iuser[]>(`${environment.server_url}`)
  
}

  getUser(id):Observable<Iuser> {
    return this.http.get<Iuser>(`${environment.server_url}/${id}`)
  }
}
