import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ipost } from '../../users/shared/ipost';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }


  getPosts():Observable<Ipost[]> {
    return this.http.get<Ipost[]>(`${environment.server1_url}`)
  
}


}
