import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { FormlyFieldConfig, FormlyFormOptions } from '@ngx-formly/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
form=new FormGroup({})
model:any={}
options:FormlyFormOptions={}
fields:FormlyFieldConfig[]=[
  {
    key: 'username',
    type: 'input',
    templateOptions: {
      label: 'Username',
      placeholder: 'username or email',
      description: 'es@gmail.com',
      required: true
    },
  },
  {
    key: 'lastname',
    type: 'input',
    templateOptions: {
      label: 'lastname',
      placeholder: 'lastname or email',
      required: true
    },
    hideExpression:'model.username'
},
{
  key: 'price',
  type: 'input',
  templateOptions: {
    type: 'number',
    min: 1,
    label: 'price',
    placeholder: 'price',
    required: true
  },
},


{
validators:{
  validation: [
    { name: 'fieldMatch', options: { errorPath: 'passwordConfirm' } },
  ],
},
  
fieldGroup: [
  {
    key: 'password',
    type: 'input',
    templateOptions: {
      type: 'password',
      minLength: 3,
      label: 'password',
      placeholder: 'Must be atleast 3 characters',
      required: true
    },
  },
  {
    key: 'confirmPassword',
    type: 'input',
    templateOptions: {
      type: 'password',
      label: 'password',
      placeholder: 'Please re-enter your password',
      required: true
    },
  }
]
}
]

  constructor() { }

  ngOnInit(): void {
  }
submit(form:any) {
  console.log(form.value);
  let userDetails = {
    username: form.value.Username,
    password: form.value.password
  }
  this.form.reset()
}
}
