import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../../service/user.service';
import { Iuser } from '../../shared/iuser';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {
user:Iuser={
  id: "",
  name: "",
  email: "",
  username: "",
  website: "",
  phone: "",
  address: {
    city: "",
    geo:{
      lat: "",
      lng: ""
  },
  street: "",
  suite: "",
  zipcode: ""
  },
  company: {
    name: "",
    bs: "",
    catchPhrase: ""
  }
}
  constructor(private route:ActivatedRoute, private userService:UserService) { }

  ngOnInit(): void {
    this.getParams();
  }


  getParams(){
    let userId = this.route.snapshot.paramMap.get("id")
    this.userService.getUser(userId).subscribe((res) => {
      this.user=res
      console.log(this.user)
    })
  }


}
