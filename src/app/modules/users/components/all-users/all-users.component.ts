import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../service/user.service';
import { Iuser } from '../../shared/iuser';

@Component({
  selector: 'app-all-users',
  templateUrl: './all-users.component.html',
  styleUrls: ['./all-users.component.scss']
})
export class AllUsersComponent implements OnInit {
users:Iuser[]=[];


  constructor(private userService: UserService, private router:Router) { }

  ngOnInit(): void {
    this.getUsers()
  }

  getUsers() {
    this.userService.getUsers().subscribe(
      (res)=> {
        this.users=res
        console.log(this.users)
      }
    )
  }



  viewUser(user: Iuser){
    this.router.navigate([`users/${user.id}/${user.username}/details`])
  }

}
