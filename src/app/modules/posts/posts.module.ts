import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostsRoutingModule } from './posts-routing.module';
import { EditPostComponent } from './components/edit-post/edit-post.component';
import { AllPostsComponent } from './components/all-posts/all-posts.component';
import { PostDetailsComponent } from './components/post-details/post-details.component';


@NgModule({
  declarations: [EditPostComponent, AllPostsComponent, PostDetailsComponent],
  imports: [
    CommonModule,
    PostsRoutingModule
  ]
})
export class PostsModule { }
